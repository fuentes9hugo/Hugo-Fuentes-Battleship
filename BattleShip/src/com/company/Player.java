package com.company;

public class Player {

    private Board board;

    private Board shotBoard;

    Player() {

        this.board = new Board();

        this.shotBoard = new Board();
    }

    public Board getBoard() {

        return this.board;
    }

    public Board getShotBoard() {

        return this.shotBoard;
    }
}
