package com.company;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {

    private Player player;

    private Computer computer;

    Game(Player player, Computer computer) {

        this.player = player;
        this.computer = computer;
    }

    public void startGame() {

        System.out.println("Bienvenido a Hundir la Flota!\n");

        System.out.println("Voy a distribuir los barcos...\n");

        this.player.getBoard().printBoard();
        this.player.getBoard().randomBoatDealer();
        this.player.getShotBoard().printBoard();

        this.computer.getBoard().printBoard();
        this.computer.getBoard().randomBoatDealer();
        this.computer.getShotBoard().printBoard();








        this.computer.getBoard().showBoard();








        System.out.println("Ya está. Podemos empezar:\n");

        showPlayerBoards();

        int[] playerShot;

        int[] computerShot;

        String computerPlay;

        int secondLetter;

        do {

            playerShot = playerShot();

            this.computer.getBoard().enemyShot(playerShot);

            this.player.getShotBoard().shotBoard(playerShot, this.computer.getBoard().touched(playerShot));

            System.out.println("TABLERO DE DISPAROS\n");

            this.player.getShotBoard().showBoard();

            System.out.println();

            computerShot = computerShot();

            secondLetter = computerShot[0] + 1;

            if (computerShot[1] == 0) {

                computerPlay = "A" + secondLetter;

            } else if (computerShot[1] == 1){

                computerPlay = "B" + secondLetter;

            } else if (computerShot[1] == 2){

                computerPlay = "C" + secondLetter;

            } else if (computerShot[1] == 3){

                computerPlay = "D" + secondLetter;

            } else if (computerShot[1] == 4){

                computerPlay = "E" + secondLetter;

            } else if (computerShot[1] == 5){

                computerPlay = "F" + secondLetter;

            } else if (computerShot[1] == 6){

                computerPlay = "G" + secondLetter;

            } else if (computerShot[1] == 7){

                computerPlay = "H" + secondLetter;

            } else if (computerShot[1] == 8){

                computerPlay = "I" + secondLetter;

            } else {

                computerPlay = "J" + secondLetter;
            }

            System.out.printf("Mi jugada es: %s\n\n", computerPlay);

            this.player.getBoard().enemyShot(computerShot);

            this.computer.getShotBoard().shotBoard(computerShot, this.player.getBoard().touched(computerShot));

            System.out.println("TABLERO DE TU FLOTA\n");

            this.player.getBoard().showBoard();

            System.out.println();

        }while(!this.player.getBoard().checkBoats() && !this.computer.getBoard().checkBoats());

        if (this.player.getBoard().checkBoats()) {

            System.out.println("Has perdido.");

        } else if (this.computer.getBoard().checkBoats()) {

            System.out.println("Has ganado!");
        }
    }

    private void showPlayerBoards() {

        System.out.println("TABLERO DE DISPAROS\n");

        this.player.getShotBoard().showBoard();

        System.out.println();

        System.out.println("TABLERO DE TU FLOTA\n");

        this.player.getBoard().showBoard();

        System.out.println();
    }

    private int[] playerShot() {

        Scanner sc = new Scanner(System.in);

        String position;

        ArrayList<String> repeatedPositions = new ArrayList<>();

        int[] playerShot = new int[2];

        int checkRepeatedPosition;

        do {

            checkRepeatedPosition = 0;

            System.out.println("> Introduce jugada (ej. A5):");

            position = sc.next();

            for (int i = 0; i < repeatedPositions.size(); i++) {

                if (position.toUpperCase().equals(repeatedPositions.get(i))) {

                    System.out.println("Esa posicion ya ha sido elegida anteriormente.\n");

                    checkRepeatedPosition++;
                }
            }
        }while (checkRepeatedPosition > 0);

        repeatedPositions.add(position.toUpperCase());

        if (position.length() == 2) {

            playerShot[0] = (int)position.charAt(1) - 49;

            playerShot[1] = (int)position.toUpperCase().charAt(0) - 65;

        } else if ((position.length() == 3) && (position.charAt(1) == 49 && position.charAt(2) == 48)){

            playerShot[0] = 9;

            playerShot[1] = (int)position.toUpperCase().charAt(0) - 65;
        }

        return playerShot;
    }

    private int[] computerShot() {

        Random rnd = new Random();

        int[] computerShot = new int[2];

        ArrayList<int[]> repeatedPositions = new ArrayList<>();

        int checkRepeatedPosition;

        do {

            checkRepeatedPosition = 0;

            computerShot[0] = rnd.nextInt(9+1);

            computerShot[1] = rnd.nextInt(9+1);

            for (int i = 0; i < repeatedPositions.size(); i++) {

                if (computerShot.equals(repeatedPositions.get(i))) {

                    checkRepeatedPosition++;
                }
            }
        }while (checkRepeatedPosition > 0);

        repeatedPositions.add(computerShot);

        return computerShot;
    }
}
